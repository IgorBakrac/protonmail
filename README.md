AUTOMATED TEST SCRIPTS FOR FOLDERS/LABELS SETTINGS

STRUCTURE OF THE CODE
-------------------------------------------------------------------------------------------------

***JAVA CLASSES***

(1) TestData
- Setting up the URL path to our working environment

(2) Driver
- Setting up the headless functionality

(3) Folders
- Methods for add, delete, edit, drag & drop, disable/enable functionality for both labels and folders

(4) Links
- Checking the "Learn more" link 

(5) Login
- Login and checking if current URL path corresponds to correct screen

-------------------------------------------------------------------------------------------------

***TEST SCRIPTS***

(1) CheckLink
- Set up Webdriver
- Running methods from Links pom class

(2) FoldersTest
- Set up Webdriver
- Running methods from Folders for add, edit, drag & drop, disable/enable notification for folder

(3) LabelsTest
- Set up Webdriver
- Running methods from Folders for add, edit, drag & drop, disable/enable notification for label

-------------------------------------------------------------------------------------------------

***SUITE*** 

"Settings" suite with 3 tests included:

(1) Check learn more link
-Runs CheckLink class

(2) Check label
-Runs LabelsTest class

(3)Check folder
-Runs FoldersTest class

-------------------------------------------------------------------------------------------------

NOTE: CI pipeline is handled via headless functionality.






