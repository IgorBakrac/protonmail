package settings;

import drivers.Drivers;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom.Folders;
import pom.Login;

public class FoldersTest {

    private WebDriver driver;
    private Folders folders;
    private Login login;

    @BeforeTest
    public void setUp() throws InterruptedException {
      System.setProperty("webdriver.chrome.driver", "/C:/Users/Stefanija/Downloads/chromedriver_win32 (1)/chromedriver.exe/");

        driver = Drivers.getDriver();
        login = new Login(driver);
        folders = new Folders(driver);
        login.logInToLabels();
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    @Test(priority = 0)
    public void checkAddFolder() {
        folders.addFolder("Igor");
    }

    @Test(priority = 1)
    public void reOrderFolder() throws InterruptedException {
        folders.dragAndDrop("Igor");
    }

    @Test(priority = 2)
    public void editFolder() throws InterruptedException {
        folders.edit("Bakraceski");
    }

    @Test(priority = 3)
    public void disableNotification() throws InterruptedException {
        folders.disableNotification();
    }

    @Test(priority = 4)
    public void delete() throws InterruptedException {
        folders.delete();
    }

}
