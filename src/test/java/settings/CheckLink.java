package settings;

import drivers.Drivers;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pom.LInks;
import pom.Login;

public class CheckLink {

    private WebDriver driver;
    private Login login;
    private LInks links;

    @BeforeTest
    public void setUp() throws InterruptedException {

       System.setProperty("webdriver.chrome.driver", "/C:/Users/Stefanija/Downloads/chromedriver_win32 (1)/chromedriver.exe/");

        driver = Drivers.getDriver();
        login = new Login(driver);
        links = new LInks(driver);
        login.logInToLabels();
    }

    @Test
    public void goToPage() {
        links.checkLink();

    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}
