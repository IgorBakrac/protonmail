package drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Drivers {

    private WebDriver driver;

    public Drivers(WebDriver driver) {
        this.driver = driver;
    }

    public static WebDriver getDriver() {
            ChromeOptions options = new ChromeOptions();

            if ("true".equals(System.getProperty("headless"))) {
                options.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
            }

            return new ChromeDriver(options);
        }
    }

