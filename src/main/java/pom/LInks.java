package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

import static org.testng.Assert.assertEquals;

public class LInks {

    private WebDriver driver;

    By learnMoreLinkLocator = By.linkText("Learn more");
    String learnMoreLink = "https://protonmail.com/support/knowledge-base/creating-folders/";

    public LInks(WebDriver driver) {
        this.driver = driver;
    }

    public void switchTabAndCheckUrl(String url) {
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        assertEquals(driver.getCurrentUrl(), url);
        driver.close();
        driver.switchTo().window(tabs.get(0));
    }

    public void checkLink() {
        driver.findElement(learnMoreLinkLocator).click();
        switchTabAndCheckUrl(learnMoreLink);
    }
}
