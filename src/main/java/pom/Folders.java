package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import java.util.List;

public class Folders {

    private WebDriver driver;

    By addFolderLocator = By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.main.flex-item-fluid.main-area > div > main > div > section > div:nth-child(3) > button.pm-button.pm-button--primary.mr1");
    By addLabelLocator = By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.main.flex-item-fluid.main-area > div > main > div > section > div:nth-child(3) > button:nth-child(2)");
    By nameInputLocator = By.id("accountName");
    By orangeLocator = By.cssSelector("body > div.pm-modalContainer > dialog > form > div > div > div:nth-child(2) > div > ul > li:nth-child(17) > input");
    By greenLocator = By.cssSelector("body > div.pm-modalContainer > dialog > form > div > div > div:nth-child(2) > div > ul > li:nth-child(13) > input");
    By submitFolderLocator = By.cssSelector("body > div.pm-modalContainer > dialog > form > footer > button.pm-button.pm-button--primary");
    By editButton = By.xpath("//button[@data-test-id='folders/labels:item-edit']");
    By dropDownDelete = By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.main.flex-item-fluid.main-area > div > main > div > section > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > button.flex-item-noshrink.pm-button.pm-group-button.pm-button--for-icon.pm-button--small");
    By deleteButtonList = By.cssSelector(".dropDown-content");
    By confirmDeleteList = By.xpath("//button[@class='pm-button pm-button--primary']");
    String disableNotification = "body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.main.flex-item-fluid.main-area > div > main > div > section > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > span > label > span:nth-child(1)";

    public Folders(WebDriver driver) {
        this.driver = driver;
    }

    public void handleAjaxAndClickElementByCssSelector(String locator) throws InterruptedException {
        for (int i = 0; i <= 5; i++) {
            Thread.sleep(1500);
            try {
                driver.findElement(By.cssSelector(locator)).click();
                break;
            } catch (Exception e) {
            }
        }
    }

    public void handleAjaxAndClickElementByLocator(By locator) throws InterruptedException {
        for (int i = 0; i <= 5; i++) {
            Thread.sleep(1500);
            try {
                driver.findElement(locator).click();
                break;
            } catch (Exception e) {
            }
        }
    }

    public void handleAjaxAndClickElementByWebElement(By locator) throws InterruptedException {
        for (int i = 0; i <= 5; i++) {
            Thread.sleep(1500);
            try {
                List<WebElement> deleteDropDowns = driver.findElements(locator);
                deleteDropDowns.get(1).click();
                break;
            } catch (Exception e) {
            }
        }
    }

    public void dragAndDrop(String name) throws InterruptedException {
        handleAjaxAndClickElementByCssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > " +
                "div > div.main.flex-item-fluid.main-area > div > main > div > section > table > tbody > " +
                "tr:nth-child(1) > td:nth-child(2) > div > span");
        for(int i = 1; i <= 20; i++) {
            if (driver.findElement(By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > " +
                    "div > div.main.flex-item-fluid.main-area > div > main > div > section > table > tbody > " +
                    "tr:nth-child(1) > td:nth-child(2) > div > span")).getText().equals(name)) {
                break;
            } else if (driver.findElement(By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.main." +
                    "flex-item-fluid.main-area > div > main > div > section > table > tbody > tr:nth-child(" + i + ") > " +
                    "td:nth-child(2) > div > span")).getText().equals(name)) {
                WebElement from = driver.findElement(By.cssSelector("body > div.app-root > div.flex.flex-nowrap." +
                        "no-scroll > div > div > div.main.flex-item-fluid.main-area > div > main > div > section > " +
                        "table > tbody > tr:nth-child(" + i + ") > td:nth-child(1) > span > svg"));
                WebElement to = driver.findElement(By.cssSelector("body > div.app-root > div.flex.flex-nowrap.no-scroll " +
                        "> div > div > div.main.flex-item-fluid.main-area > div > main > div > section > table > tbody " +
                        "> tr:nth-child(1) > td:nth-child(1) > span > svg"));

                Actions act=new Actions(driver);
                act.dragAndDrop(from, to).build().perform();

                break;
            }
        }
    }

    public void delete() throws InterruptedException {
        handleAjaxAndClickElementByLocator(dropDownDelete);
        handleAjaxAndClickElementByWebElement(deleteButtonList);
        handleAjaxAndClickElementByWebElement(confirmDeleteList);
    }

    public void addFolder(String name) {
        driver.findElement(addFolderLocator).click();
        driver.findElement(nameInputLocator).sendKeys(name);
        driver.findElement(orangeLocator).click();
        driver.findElement(submitFolderLocator).click();
    }

    public void addLabel(String name) {
        driver.findElement(addLabelLocator).click();
        driver.findElement(nameInputLocator).sendKeys(name);
        driver.findElement(greenLocator).click();
        driver.findElement(submitFolderLocator).click();
    }

    public void disableNotification() throws InterruptedException {
        handleAjaxAndClickElementByCssSelector(disableNotification);
    }

    public void edit(String name) throws InterruptedException {
        handleAjaxAndClickElementByLocator(editButton);
        handleAjaxAndClickElementByLocator(nameInputLocator);
        driver.findElement(nameInputLocator).sendKeys(name);
        driver.findElement(submitFolderLocator).click();
    }



}
