package pom;

import data.TestData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertEquals;

public class Login {

    private WebDriver driver;

    By username = By.id("username");
    By password = By.id("password");
    By logInButton = By.id("login_btn");
    By settingsButtonLocator = By.cssSelector("#body > div > div.content.flex-item-fluid.reset4print > header > div > div > ul > li:nth-child(3)");
    String foldersButton = "body > div.app-root > div.flex.flex-nowrap.no-scroll > div > div > div.sidebar.flex.flex-column.noprint > nav > ul > li:nth-child(11)";
    String skipButton = "#secured-inbox > div.pm-modalOverlay.reset4print.ng-scope > div > dialog > div > footer > button.pm-button.ng-scope";

    public Login(WebDriver driver) {
        this.driver = driver;
    }

    public void handleAjaxAndClickElementByCssSelector(String locator) throws InterruptedException {

        for (int i = 0; i <= 5; i++) {
            Thread.sleep(1500);
            try {
                driver.findElement(By.cssSelector(locator)).click();
                break;
            } catch (Exception e) {
            }
        }
    }

    public void handleAjaxAndClickElementByLocator(By locator) throws InterruptedException {
        for (int i = 0; i <= 5; i++) {
            Thread.sleep(1500);
            try {
                driver.findElement(locator).click();
                break;
            } catch (Exception e) {
            }
        }
    }

    public void logInToLabels() throws InterruptedException {
        driver.get(TestData.URL);
        handleAjaxAndClickElementByLocator(username);
        driver.findElement(username).sendKeys("and_test_qa");
        driver.findElement(password).sendKeys("and_qa-testing");
        driver.findElement(logInButton).click();
        handleAjaxAndClickElementByCssSelector(skipButton);
        handleAjaxAndClickElementByLocator(settingsButtonLocator);
        handleAjaxAndClickElementByCssSelector(foldersButton);
        assertEquals(driver.getCurrentUrl(), "https://beta.protonmail.com/settings/labels");
    }
}
